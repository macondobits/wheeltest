/////////////////////////////////////////////////////////////////////////////
// Name:        wheeltest.cpp
// Purpose:     Wheel Test program
// Author:      Nicolay Giraldo
// Modified by:
// Created:     26/07/2021
// RCS-ID:      $Id$
// Copyright:   (c) Nicolay Giraldo
// Licence:     Proprietary
/////////////////////////////////////////////////////////////////////////////

// ============================================================================
// declarations
// ============================================================================

// ----------------------------------------------------------------------------
// headers
// ----------------------------------------------------------------------------

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

// for all others, include the necessary headers (this file is usually all you
// need because it includes almost all "standard" wxWidgets headers)
#ifndef WX_PRECOMP
	#include "wx/wx.h"
#endif

#include <wx/dcbuffer.h>
#include "device.h"

#define TIMER_ID 1000
#define PI 3.14159265

device dxdevice;
wxBitmap devicetypebitmap(64, 64, -1);
wxBitmap buttononbitmap(48, 48, -1);
wxBitmap buttonoffbitmap(48, 48, -1);
wxBitmap buttonstatebitmap(48, 48, -1);
wxBitmap povbitmap(120, 120, -1);
wxBitmap steeringwheelbitmap(424, 424, -1);
wxBitmap steeringwheelrotatedbitmap(424, 424, -1);

HWND hWnd;

WX_DECLARE_OBJARRAY(wxStaticBitmap*, wxStaticBitmapPointerArray);
WX_DECLARE_OBJARRAY(wxSlider*, wxSliderPointerArray);

#include <wx/arrimpl.cpp>

WX_DEFINE_OBJARRAY(wxStaticBitmapPointerArray);
WX_DEFINE_OBJARRAY(wxSliderPointerArray);

wxStaticBitmapPointerArray buttons;
wxSliderPointerArray axis;
wxStaticBitmapPointerArray povs;

wxArrayString axistypes;
wxStaticText* wheeldesc;
wxStaticBitmap *steeringwheel;

bool buttonstates [128];
int povstates [4];
bool iswheel = false;
int wheeldegrees = 270;

// ----------------------------------------------------------------------------
// resources
// ----------------------------------------------------------------------------

// the application icon (under Windows and OS/2 it is in resources and even
// though we could still include the XPM here it would be unused)
#ifndef wxHAS_IMAGES_IN_RESOURCES
	#include "resources/sample.xpm"
#endif

// ----------------------------------------------------------------------------
// private classes
// ----------------------------------------------------------------------------

// Define a new application type, each program should derive a class from wxApp
class MyApp : public wxApp
{
public:
	// override base class virtuals
	// ----------------------------

	// this one is called on application startup and is a good place for the app
	// initialization (doing it here and not in the ctor allows to have an error
	// return: if OnInit() returns false, the application terminates)
	virtual bool OnInit();
};

// Define a new frame type: this is going to be our main frame
class WindowFrame : public wxFrame
{
public:
	// ctor(s)
	WindowFrame(const wxString& title);

	// event handlers (these functions should _not_ be virtual)
	void OnQuit(wxCommandEvent& event);
	void OnAbout(wxCommandEvent& event);

	void OnTimer(wxTimerEvent& event);
	void OnIdle(wxIdleEvent& event);

private:
	// any class wishing to process wxWidgets events must use this macro
	wxDECLARE_EVENT_TABLE();
	wxTimer m_timer;

	unsigned int numaxis;
	unsigned int numbuttons;
	unsigned int numpovs;
};

// ----------------------------------------------------------------------------
// constants
// ----------------------------------------------------------------------------

// IDs for the controls and the menu commands
enum
{
	// menu items
	WheelTest_Quit = wxID_EXIT,

	// it is important for the id corresponding to the "About" command to have
	// this standard value as otherwise it won't be handled properly under Mac
	// (where it is special and put into the "Apple" menu)
	WheelTest_About = wxID_ABOUT
};

// ----------------------------------------------------------------------------
// event tables and other macros for wxWidgets
// ----------------------------------------------------------------------------

// the event tables connect the wxWidgets events with the functions (event
// handlers) which process them. It can be also done at run-time, but for the
// simple menu events like this the static method is much simpler.
wxBEGIN_EVENT_TABLE(WindowFrame, wxFrame)
	EVT_MENU(WheelTest_Quit,  WindowFrame::OnQuit)
	EVT_MENU(WheelTest_About, WindowFrame::OnAbout)
	EVT_TIMER(TIMER_ID, WindowFrame::OnTimer)
	EVT_IDLE(WindowFrame::OnIdle)
wxEND_EVENT_TABLE()

// Create a new application object: this macro will allow wxWidgets to create
// the application object during program execution (it's better than using a
// static object for many reasons) and also implements the accessor function
// wxGetApp() which will return the reference of the right type (i.e. MyApp and
// not wxApp)
IMPLEMENT_APP(MyApp)

// ============================================================================
// implementation
// ============================================================================

// ----------------------------------------------------------------------------
// the application class
// ----------------------------------------------------------------------------

// 'Main program' equivalent: the program execution "starts" here
bool MyApp::OnInit()
{
	// call the base class initialization method, currently it only parses a
	// few common command-line options but it could be do more in the future
	if ( !wxApp::OnInit() )
		return false;

	wxImage::AddHandler( new wxPNGHandler );

	// create the main application window
	WindowFrame *frame = new WindowFrame("WheelTest App");

	hWnd = frame->GetHandle();

	// and show it (the frames, unlike simple controls, are not shown when
	// created initially)
	frame->Show(true);

	if (dxdevice.hasJoystick()) {
		dxdevice.getCapabilities();
		wxString devname = dxdevice.getTypeName();
		wxString newstatus;
		newstatus.Printf( wxT("Control %s has been detected."), devname);
		frame->SetStatusText(newstatus);
	} else {
		frame->SetStatusText("No control detected.");
	}

	if (devInstanceName.find(wxString("Logitech G25")) != wxNOT_FOUND){
		wheeldegrees = 900;
	}
	if (devInstanceName.find(wxString("Logitech G27")) != wxNOT_FOUND){
		wheeldegrees = 875;
	}
	if (devInstanceName.find(wxString("Logitech G29")) != wxNOT_FOUND){
		wheeldegrees = 900;
	}
	if (devInstanceName.find(wxString("Logitech G920")) != wxNOT_FOUND){
		wheeldegrees = 900;
	}
	if (devInstanceName.find(wxString("Logitech G923")) != wxNOT_FOUND){
		wheeldegrees = 900;
	}

	// success: wxApp::OnRun() will be called which will enter the main message
	// loop and the application will run. If we returned false here, the
	// application would exit immediately.
	return true;
}

// ----------------------------------------------------------------------------
// main frame
// ----------------------------------------------------------------------------

// frame constructor
WindowFrame::WindowFrame(const wxString& title)
	: wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(900, 800)), m_timer(this, TIMER_ID)
{

	numaxis = 0;
	numbuttons = 0;
	numpovs = 0;

	// set the frame icon
	SetIcon(wxICON(sample));

	m_timer.Start(11);

#if wxUSE_MENUS
	// create a menu bar
	wxMenu *fileMenu = new wxMenu;

	// the "About" item should be in the help menu
	wxMenu *helpMenu = new wxMenu;
	helpMenu->Append(WheelTest_About, "&About\tF1", "Show about dialog");

	fileMenu->Append(WheelTest_Quit, "E&xit\tAlt-X", "Quit this program");

	// now append the freshly created menu to the menu bar...
	wxMenuBar *menuBar = new wxMenuBar();
	menuBar->Append(fileMenu, "&File");
	menuBar->Append(helpMenu, "&Help");

	// ... and attach this menu bar to the frame
	SetMenuBar(menuBar);
#endif // wxUSE_MENUS

	wxPanel* panel = new wxPanel(this, wxID_ANY);

	wxFlexGridSizer *mainsizer = new wxFlexGridSizer(2, 1, 0, 0);
	mainsizer->AddGrowableRow(1);

	wxFlexGridSizer *topsizer = new wxFlexGridSizer(1, 3, 0, 0);
	topsizer->AddGrowableCol(1);
	topsizer->AddGrowableCol(2);

	devicetypebitmap = wxBITMAP_PNG(nousb_png);
	buttononbitmap = wxBITMAP_PNG(btn_on_png);
	buttonoffbitmap = wxBITMAP_PNG(btn_off_png);
	povbitmap = wxBITMAP_PNG(dpad_png);
	steeringwheelbitmap = wxBITMAP_PNG(steeringwheel_png);
	wxString devname("No control detected");
	wxString subname;
	wxString snumaxis;
	wxString snumbuttons;
	wxString snumpovs;
	wxString hasffb;
	wxString needspolling;

	if (dxdevice.hasJoystick()) {
		dxdevice.getCapabilities();
		devname = dxdevice.getTypeName();
		subname = dxdevice.getSubTypeName();

		numaxis = (unsigned int)dxdevice.getAxes();
		numbuttons = (unsigned int)dxdevice.getButtons();
		numpovs = (unsigned int)dxdevice.getPOVs();

		snumaxis.Printf( wxT("Controller has %u axis."), numaxis);
		snumbuttons.Printf( wxT("Controller has %u buttons."), numbuttons);
		snumpovs.Printf( wxT("Controller has %u POVs."), numpovs);
		if (dxdevice.hasForceFeedback()) {
			hasffb = wxString("Device has FFB");
		} else {
			hasffb = wxString("No FFB");
		}
		if (dxdevice.needsPolling()) {
			needspolling = wxString("Device requires polling data");
		} else {
			needspolling = wxString("Device is interrupt driven");
		}

		switch (devtype) {
			case DI8DEVTYPE_1STPERSON:
				devicetypebitmap = wxBITMAP_PNG(firstperson_png);
				break;
			case DI8DEVTYPE_DRIVING:
				devicetypebitmap = wxBITMAP_PNG(driving_png);
				iswheel = true;
				break;
			case DI8DEVTYPE_FLIGHT:
				devicetypebitmap = wxBITMAP_PNG(flight_png);
				break;
			case DI8DEVTYPE_GAMEPAD:
				devicetypebitmap = wxBITMAP_PNG(gamepad_png);
				break;
			case DI8DEVTYPE_JOYSTICK:
				devicetypebitmap = wxBITMAP_PNG(joystick_png);
				break;
			default:
				devicetypebitmap = wxBITMAP_PNG(usb_png);
		}

	} else {
	}

	wxStaticBitmap *devicetypestaticbitmap = new wxStaticBitmap(panel, wxID_STATIC, devicetypebitmap);
	topsizer->Add(devicetypestaticbitmap, 0, wxALIGN_RIGHT|wxALL|wxEXPAND, 5);

	wxStaticText* controllerinstancename = new wxStaticText(panel, wxID_STATIC, devInstanceName, wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	// wxStaticText* controllerproductname = new wxStaticText(panel, wxID_STATIC, devProductName, wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	wxStaticText* controllertypedesc = new wxStaticText(panel, wxID_STATIC, devname, wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	wxStaticText* controllersubtypedesc = new wxStaticText(panel, wxID_STATIC, subname, wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	wxStaticText* controllernumaxisdesc = new wxStaticText(panel, wxID_STATIC, snumaxis, wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	wxStaticText* controllernumbuttonsdesc = new wxStaticText(panel, wxID_STATIC, snumbuttons, wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	wxStaticText* controllernumpovsdesc = new wxStaticText(panel, wxID_STATIC, snumpovs, wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);

	wxStaticText* controllerffbdesc = new wxStaticText(panel, wxID_STATIC, hasffb, wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	wxStaticText* controllerpollingdesc = new wxStaticText(panel, wxID_STATIC, needspolling, wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);

	wxFlexGridSizer *textssizerleft = new wxFlexGridSizer(4, 1, 0, 0);
	wxFlexGridSizer *textssizerright = new wxFlexGridSizer(4, 1, 0, 0);

	textssizerleft->Add(controllerinstancename, 0, wxALIGN_LEFT|wxALL|wxGROW, 0);
	// textssizerleft->Add(controllerproductname, 0, wxALIGN_LEFT|wxALL|wxGROW, 0);
	textssizerleft->Add(controllernumaxisdesc, 0, wxALIGN_LEFT|wxALL|wxGROW, 0);
	textssizerleft->Add(controllernumbuttonsdesc, 0, wxALIGN_LEFT|wxALL|wxGROW, 0);
	textssizerleft->Add(controllernumpovsdesc, 0, wxALIGN_LEFT|wxALL|wxGROW, 0);

	textssizerright->Add(controllertypedesc, 0, wxALIGN_LEFT|wxALL|wxGROW, 0);
	textssizerright->Add(controllersubtypedesc, 0, wxALIGN_LEFT|wxALL|wxGROW, 0);
	textssizerright->Add(controllerffbdesc, 0, wxALIGN_LEFT|wxALL|wxGROW, 0);
	textssizerright->Add(controllerpollingdesc, 0, wxALIGN_LEFT|wxALL|wxGROW, 0);

	wxFlexGridSizer *bottomsizer = new wxFlexGridSizer(1, 2, 0, 0);
	bottomsizer->AddGrowableCol(1);

	if (iswheel) {
		steeringwheel = new wxStaticBitmap(panel, wxID_STATIC, steeringwheelbitmap);
		bottomsizer->Add(steeringwheel, wxID_STATIC, wxALIGN_LEFT|wxLEFT|wxRIGHT|wxGROW, 20);
	}

	wxFlexGridSizer *controlssizer = new wxFlexGridSizer(3, 1, 0, 0);
	controlssizer->AddGrowableCol(0);

	wxFlexGridSizer *axissizer = new wxFlexGridSizer(20, 1, 0, 0);
	axissizer->AddGrowableCol(0);

	wxFlexGridSizer *buttonssizer = new wxFlexGridSizer(10, 6, 0, 0);
	buttonssizer->AddGrowableCol(0);
	buttonssizer->AddGrowableCol(1);
	buttonssizer->AddGrowableCol(2);
	buttonssizer->AddGrowableCol(3);
	buttonssizer->AddGrowableCol(4);

	wxFlexGridSizer *povsssizer = new wxFlexGridSizer(10, 2, 0, 0);
	povsssizer->AddGrowableCol(0);
	povsssizer->AddGrowableCol(1);

	if (numaxis > 0) {
		int slidercounter = 0;
		for (unsigned int i = 0; i < numaxis; ++i)
		{
			DIDEVICEOBJECTINSTANCE& objectinstance = devObjects.Item(i);
			wxString axisname;
			if (objectinstance.guidType == GUID_XAxis) {
				axisname = "XAxis";
			}
			if (objectinstance.guidType == GUID_YAxis) {
				axisname = "YAxis";
			}
			if (objectinstance.guidType == GUID_ZAxis) {
				axisname = "ZAxis";
			}
			if (objectinstance.guidType == GUID_RxAxis) {
				axisname = "RxAxis";
			}
			if (objectinstance.guidType == GUID_RyAxis) {
				axisname = "RyAxis";
			}
			if (objectinstance.guidType == GUID_RzAxis) {
				axisname = "RzAxis";
			}
			if (objectinstance.guidType == GUID_Slider) {
				axisname = "Slider";
			}
			if (objectinstance.guidType == GUID_Button) {
				axisname = "Button";
			}
			if (objectinstance.guidType == GUID_Key) {
				axisname = "Key";
			}
			if (objectinstance.guidType == GUID_POV) {
				axisname = "POV";
			}
			if (objectinstance.guidType == GUID_Unknown) {
				axisname = "Unknown";
			}
			if (axisname == "Slider") {
				wxString slidername;
				slidername.Printf("%d", slidercounter++);
				axisname += slidername;
			}
			axistypes.Add(axisname);
			wxString axisdesc(objectinstance.tszName);
			// axisdesc += " | " + axisname;
			// axisname.Printf("%d", DIDFT_GETINSTANCE(objectinstance.dwOfs));
			// axisdesc += " | " + axisname;
			// if (objectinstance.dwFlags & DIDOI_ASPECTACCEL)
			// {
			// 	axisdesc += " + acceleration information";
			// }
			// if (objectinstance.dwFlags & DIDOI_ASPECTFORCE)
			// {
			// 	axisdesc += " + force information";
			// }
			// if (objectinstance.dwFlags & DIDOI_ASPECTMASK)
			// {
			// 	axisdesc += " + aspect information";
			// }
			// if (objectinstance.dwFlags & DIDOI_ASPECTPOSITION)
			// {
			// 	axisdesc += " + position information";
			// }
			// if (objectinstance.dwFlags & DIDOI_ASPECTVELOCITY)
			// {
			// 	axisdesc += " + velocity information";
			// }
			if (objectinstance.dwFlags & DIDOI_FFACTUATOR)
			{
				axisdesc += " FFB";
			}
			// if (objectinstance.dwFlags & DIDOI_FFEFFECTTRIGGER)
			// {
			// 	axisdesc += " + can trigger playback of force-feedback effects";
			// }
			// if (objectinstance.dwFlags & DIDOI_GUIDISUSAGE)
			// {
			// 	axisdesc += " + packed DWORD usage";
			// }
			// if (objectinstance.dwFlags & DIDOI_POLLED)
			// {
			// 	axisdesc += " + does not return data until polled";
			// }
			wxSlider *slider = new wxSlider(panel, wxID_STATIC+numaxis, 0, 0, 65536, wxDefaultPosition, wxDefaultSize, wxGA_HORIZONTAL|wxSL_BOTH, wxDefaultValidator, axisname);
			slider->SetThumbLength(24);
			wxStaticText* sliderdesc = new wxStaticText(panel, wxID_STATIC, axisdesc, wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
			if (axisname == "XAxis") {
				wheeldesc = sliderdesc;
			}
			axissizer->Add(sliderdesc, 0, wxALL|wxGROW, 0);
			axissizer->Add(slider, 0, wxBOTTOM|wxGROW, 7);
			axis.Add(slider);
		}
	}

	if (numbuttons > 0) {
		for (unsigned int i = 0; i < numbuttons; ++i)
		{
			buttonoffbitmap = wxBITMAP_PNG(btn_off_png);
			wxMemoryDC dc;
			dc.SelectObject(buttonoffbitmap);
			wxString butndesc;
			butndesc.Printf("%d", i+1);
			wxFont font(wxFontInfo(13).FaceName("Arial"));
			dc.SetFont(font);
			dc.SetTextForeground(*wxCYAN); // This is for some reason not working
			dc.DrawText(butndesc, (i<9?19:14), 15);
			dc.SetFont(wxNullFont);
			wxStaticBitmap *buttonbitmap = new wxStaticBitmap(panel, wxID_STATIC, buttonoffbitmap);
			buttonssizer->Add(buttonbitmap, 0, wxALL|wxGROW, 5);
			buttons.Add(buttonbitmap);
			buttonstates[i] = false;
		}
	}

	if (numpovs > 0) {
		for (unsigned int i = 0; i < numpovs; ++i)
		{
			wxStaticBitmap *dpadbitmap = new wxStaticBitmap(panel, wxID_STATIC, povbitmap);
			povsssizer->Add(dpadbitmap, 0, wxALL|wxGROW, 5);
			povs.Add(dpadbitmap);
			povstates[i] = 0;
		}
	}

	controlssizer->Add(axissizer, 0, wxALL|wxGROW, 0);
	controlssizer->Add(buttonssizer, 0, wxALL|wxGROW, 0);
	controlssizer->Add(povsssizer, 0, wxALL|wxGROW, 0);

	bottomsizer->Add(controlssizer, 0, wxALL|wxGROW, 5);

	topsizer->Add(textssizerleft, 0, wxALIGN_LEFT|wxALL|wxGROW, 5);
	topsizer->Add(textssizerright, 0, wxALIGN_LEFT|wxALL|wxGROW, 5);

	mainsizer->Add(topsizer, 0, wxALIGN_LEFT|wxALL|wxGROW, 0);
	mainsizer->Add(bottomsizer, 0, wxALIGN_LEFT|wxALL|wxGROW, 0);

	panel->SetSizer(mainsizer);
	mainsizer->SetSizeHints( this );
	mainsizer->Fit( this );

#if wxUSE_STATUSBAR
	// create a status bar just for fun (by default with 1 pane only)
	CreateStatusBar(1);
	SetStatusText("Wheel Test utility");
#endif // wxUSE_STATUSBAR
}


// event handlers

void WindowFrame::OnQuit(wxCommandEvent& WXUNUSED(event))
{
	// true is to force the frame to close
	Close(true);
}

void WindowFrame::OnAbout(wxCommandEvent& WXUNUSED(event))
{
	wxMessageBox(wxString::Format
				 (
					"This is the Wheel Test Utility!\n"
					"by Nicolay Giraldo\n"
					"\n"
					"Use this utility to test the inputs \n"
					"of a steering wheel."
				 ),
				 "About WheelTest utility",
				 wxOK | wxICON_INFORMATION,
				 this);
}

void WindowFrame::OnTimer(wxTimerEvent& event)
{
	dxdevice.readData();

}

void WindowFrame::OnIdle(wxIdleEvent& event)
{

	// if (rand() % 5) return; //stupid hack to reduce flicker

	if (numbuttons > 0) {
		for (unsigned int i = 0; i < numbuttons; ++i)
		{
			bool newstate = joystickState.rgbButtons[i] ? true : false;
			if (newstate != buttonstates[i]) {
				buttonstatebitmap = joystickState.rgbButtons[i] ? wxBITMAP_PNG(btn_on_png) : wxBITMAP_PNG(btn_off_png);
				wxBufferedDC dc;
				dc.SelectObject(buttonstatebitmap);
				wxString butndesc;
				butndesc.Printf("%d", i+1);
				wxFont font(wxFontInfo(13).FaceName("Arial"));
				dc.SetFont(font);
				dc.SetTextForeground(*wxCYAN); // This is for some reason not working
				dc.DrawText(butndesc, (i<9?19:14), 15);
				dc.SetFont(wxNullFont);
				wxStaticBitmap *buttonbitmap = buttons.Item(i);
				buttonbitmap->SetBitmap(buttonstatebitmap);
			}
			buttonstates[i] = newstate;
		}
	}

	if (numpovs > 0) {
		for (unsigned int i = 0; i < numpovs; ++i)
		{
			int newstate = joystickState.rgdwPOV[i];
			if (newstate != povstates[i]) {
				int degrees = newstate/100;
				int xcoord = 60;
				int ycoord = 60;
				povbitmap = wxBITMAP_PNG(dpad_png);
				wxBufferedDC dc;
				dc.SelectObject(povbitmap);

				if (newstate != 65535 && newstate != -1)
				{
					xcoord = (sin(degrees*PI/180) * 40) + 60;
					ycoord = -(cos(degrees*PI/180) * 40) + 60;
				}

				dc.DrawCircle(xcoord, ycoord, 5);

				wxStaticBitmap *dpadbitmap = povs.Item(i);
				dpadbitmap->SetBitmap(povbitmap);
			}
			povstates[i] = newstate;
		}
	}

	if (numaxis > 0) {
		for (unsigned int i = 0; i < numaxis; ++i)
		{
			wxString axisname = axistypes[i];
			wxSlider *slider = axis[i];
			if (axisname == "XAxis") {
				int oldvalue = slider->GetValue();
				if (oldvalue != joystickState.lX)
				{
					slider->SetValue(joystickState.lX);
					if (iswheel) {
						double conversionfactor = (360*32768)/(PI*wheeldegrees);
						steeringwheelbitmap = wxBITMAP_PNG(steeringwheel_png);
						wxImage steeringwheelimage = steeringwheelbitmap.ConvertToImage();
						steeringwheelimage.Rescale(424, 424);
						// steeringwheelimage.SetRGB(212, 212, 255, 0, 0);
						wxImage steeringwheelroatatedimage = steeringwheelimage.Rotate((32768-joystickState.lX)/conversionfactor, wxPoint(212, 212));
						wxSize currentsize = steeringwheelroatatedimage.GetSize();
						currentsize.DecBy(424, 424);
						steeringwheelimage = steeringwheelroatatedimage.Resize(wxSize(424, 424), wxPoint(-currentsize.GetWidth()/2, -currentsize.GetHeight()/2));
						wxBitmap steeringwheelrotatedbitmap(steeringwheelimage);
						steeringwheel->SetBitmap(steeringwheelrotatedbitmap);
					}
				}
			}
			if (axisname == "YAxis") {
				int oldvalue = slider->GetValue();
				if (oldvalue != joystickState.lY)
				{
					slider->SetValue(joystickState.lY);
				}
			}
			if (axisname == "ZAxis") {
				int oldvalue = slider->GetValue();
				if (oldvalue != joystickState.lZ)
				{
					slider->SetValue(joystickState.lZ);
				}
			}
			if (axisname == "RxAxis") {
				int oldvalue = slider->GetValue();
				if (oldvalue != joystickState.lRx)
				{
					slider->SetValue(joystickState.lRx);
				}
			}
			if (axisname == "RyAxis") {
				int oldvalue = slider->GetValue();
				if (oldvalue != joystickState.lRy)
				{
					slider->SetValue(joystickState.lRy);
				}
			}
			if (axisname == "RzAxis") {
				int oldvalue = slider->GetValue();
				if (oldvalue != joystickState.lRz)
				{
					slider->SetValue(joystickState.lRz);
				}
			}
			if (axisname == "Slider0") {
				int oldvalue = slider->GetValue();
				if (oldvalue != joystickState.rglSlider[0])
				{
					slider->SetValue(joystickState.rglSlider[0]);
				}
			}
			if (axisname == "Slider1") {
				int oldvalue = slider->GetValue();
				if (oldvalue != joystickState.rglSlider[1])
				{
					slider->SetValue(joystickState.rglSlider[1]);
				}
			}
			// if (axisname == "Button") {
			// 	slider->SetValue(joystickState.asdf);
			// }
			// if (axisname == "Key") {
			// 	slider->SetValue(joystickState.asdf);
			// }
			// if (axisname == "POV") {
			// 	slider->SetValue(joystickState.asdf);
			// }
		}
	}

	event.Skip();
}
