#include <cstdlib>
#include "device.h"
#include <wx/arrimpl.cpp>

#ifdef _WIN32
LPDIRECTINPUT        lpDInput       = NULL;	// pointer to direct input object
LPDIRECTINPUTDEVICE  lpDIKeyboard   = NULL;	// pointer to keyboard device
LPDIRECTINPUTDEVICE  lpDIMouse      = NULL;	// pointer to mouse device
LPDIRECTINPUTDEVICE8 joystick;
LPDIRECTINPUTEFFECT  effect         = NULL;
char                 keyboard[144];			// contains state of keyboard
bool                 keyevent[144];			// scan code of a pressed key
DIMOUSESTATE         MouseState;			// contains state of mouse

float                mouse_x;
float                mouse_y;

DIDEVCAPS capabilities;
DIJOYSTATE2 joystickState;
DIEFFECT diEffect;
DIPERIODIC diPer;

WX_DEFINE_OBJARRAY(devObjectArray);

LPDIRECTINPUT8 di;
HRESULT hr;

DWORD devtype;
DWORD subtype;

wxString devInstanceName;
wxString devProductName;

devObjectArray devObjects;

BOOL CALLBACK
enumCallback(const DIDEVICEINSTANCE* instance, VOID* context)
{
	HRESULT hr;

	// Obtain an interface to the enumerated joystick.
	hr = di->CreateDevice(instance->guidInstance, &joystick, NULL);

	// If it failed, then we can't use this joystick. (Maybe the user unplugged
	// it while we were in the middle of enumerating it.)
	if (FAILED(hr)) { 
		return DIENUM_CONTINUE;
	}

	devtype = GET_DIDEVICE_TYPE(instance->dwDevType);
	subtype = GET_DIDEVICE_SUBTYPE(instance->dwDevType);

	devInstanceName = wxString(instance->tszInstanceName);
	devProductName = wxString(instance->tszProductName);

	// Stop enumeration. Note: we're just taking the first joystick we get. You
	// could store all the enumerated joysticks and let the user pick.
	return DIENUM_STOP;
}

BOOL CALLBACK
enumAxesCallback(const DIDEVICEOBJECTINSTANCE* instance, VOID* context)
{
	HWND hDlg = (HWND)context;

	devObjects.Add(*instance);

	return DIENUM_CONTINUE;
}

HRESULT
poll(DIJOYSTATE2 *js)
{
	HRESULT hr;

	if (joystick == NULL) {
		return S_OK;
	}

	// Poll the device to read the current state
	hr = joystick->Poll();
	if (FAILED(hr)) {
		// DInput is telling us that the input stream has been
		// interrupted. We aren't tracking any state between polls, so
		// we don't have any special reset that needs to be done. We
		// just re-acquire and try again.
		hr = joystick->Acquire();
		while (hr == DIERR_INPUTLOST) {
			hr = joystick->Acquire();
		}

		// If we encounter a fatal error, return failure.
		if ((hr == DIERR_INVALIDPARAM) || (hr == DIERR_NOTINITIALIZED)) {
			return E_FAIL;
		}

		// If another application has control of this device, return successfully.
		// We'll just have to wait our turn to use the joystick.
		if (hr == DIERR_OTHERAPPHASPRIO) {
			return S_OK;
		}
	}

	// Get the input's device state
	if (FAILED(hr = joystick->GetDeviceState(sizeof(DIJOYSTATE2), js))) {
		return hr; // The device should have been acquired during the Poll()
	}

	return S_OK;
}

#endif

device::device(void) {
	// Create a DirectInput device
	if (FAILED(hr = DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&di, NULL))) {
		// return hr;
	}

	// Look for the first simple joystick we can find.
	if (FAILED(hr = di->EnumDevices(DI8DEVCLASS_GAMECTRL, enumCallback, NULL, DIEDFL_ATTACHEDONLY))) {
		// return hr;
	}

	dtype    = devtype;
	dsubtype = subtype;

	// Make sure we got a joystick
	if (joystick == NULL) {
		printf("Joystick not found.\n");
		// return E_FAIL;
	}
}

device::~device() {
	if (joystick) { 
		joystick->Unacquire();
		joystick->Release();
	}
}

bool
device::hasJoystick() {
	return joystick != NULL;
}

void
device::getCapabilities() {
	
	// Set the data format to "simple joystick" - a predefined data format 
	//
	// A data format specifies which controls on a device we are interested in,
	// and how they should be reported. This tells DInput that we will be
	// passing a DIJOYSTATE2 structure to IDirectInputDevice::GetDeviceState().
	if (FAILED(hr = joystick->SetDataFormat(&c_dfDIJoystick2))) {
		// return hr;
	}

	// Set the cooperative level to let DInput know how this device should
	// interact with the system and with other DInput applications.
	if (FAILED(hr = joystick->SetCooperativeLevel(hWnd, DISCL_EXCLUSIVE | DISCL_FOREGROUND))) {
		// return hr;
	}

	// Because we'll be playing force-feedback effects, disable
	// the autocentering spring.
	DIPROPDWORD dipdw;
	dipdw.diph.dwSize       = sizeof(DIPROPDWORD);
	dipdw.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	dipdw.diph.dwObj        = 0;
	dipdw.diph.dwHow        = DIPH_DEVICE;
	dipdw.dwData            = DIPROPAUTOCENTER_OFF;

	hr = joystick->SetProperty( DIPROP_AUTOCENTER, &dipdw.diph );
	// if( FAILED(hr) )
	// 	return;

	// Determine how many axis the joystick has (so we don't error out setting
	// properties for unavailable axis)
	capabilities.dwSize = sizeof(DIDEVCAPS);
	if (FAILED(hr = joystick->GetCapabilities(&capabilities))) {
		// return hr;
	}

	// Enumerate the axes of the joystick
	if (FAILED(hr = joystick->EnumObjects(enumAxesCallback, NULL, DIDFT_AXIS))) {
		// return hr;
	}
}

unsigned long
device::getSize(){
	return capabilities.dwSize;
}

unsigned long
device::getFlags(){
	return capabilities.dwFlags;
}

unsigned long
device::getDevType(){
	return capabilities.dwDevType;
}

unsigned long
device::getAxes(){
	return capabilities.dwAxes;
}

unsigned long
device::getButtons(){
	return capabilities.dwButtons;
}

unsigned long
device::getPOVs(){
	return capabilities.dwPOVs;
}

unsigned long
device::getFFSamplePeriod(){
	return capabilities.dwFFSamplePeriod;
}

unsigned long
device::getFFMinTimeResolution(){
	return capabilities.dwFFMinTimeResolution;
}

unsigned long
device::getFirmwareRevision(){
	return capabilities.dwFirmwareRevision;
}

unsigned long
device::getHardwareRevision(){
	return capabilities.dwHardwareRevision;
}

unsigned long
device::getFFDriverVersion(){
	return capabilities.dwFFDriverVersion;
}

bool
device::hasForceFeedback(){
	return capabilities.dwFlags & DIDC_FORCEFEEDBACK;
}

bool
device::needsPolling(){
	return capabilities.dwFlags & DIDC_POLLEDDEVICE;
}

wxString
device::getTypeName(){
	wxString type_name = "";
	switch (devtype) {
		case DI8DEVTYPE_1STPERSON:
			type_name = "First person controller";
			break;
		case DI8DEVTYPE_DEVICE:
			type_name = "Unknown device";
			break;
		case DI8DEVTYPE_DEVICECTRL:
			type_name = "Device to control another device";
			break;
		case DI8DEVTYPE_DRIVING:
			type_name = "Steering wheel";
			break;
		case DI8DEVTYPE_FLIGHT:
			type_name = "Flight yoke";
			break;
		case DI8DEVTYPE_GAMEPAD:
			type_name = "Gamepad";
			break;
		case DI8DEVTYPE_JOYSTICK:
			type_name = "Joystick";
			break;
		case DI8DEVTYPE_KEYBOARD:
			type_name = "Keyboard";
			break;
		case DI8DEVTYPE_MOUSE:
			type_name = "Mouse";
			break;
		case DI8DEVTYPE_REMOTE:
			type_name = "Remote control";
			break;
		case DI8DEVTYPE_SCREENPOINTER:
			type_name = "Screen pointer";
			break;
		case DI8DEVTYPE_SUPPLEMENTAL:
			type_name = "Supplemental device";
			break;
	}
	return type_name;
}

wxString
device::getSubTypeName(){
	wxString type_name = "";
	switch (devtype) {
		case DI8DEVTYPE_1STPERSON:
			switch (subtype) {
				case DI8DEVTYPE1STPERSON_LIMITED:
					type_name = "Limited";
					break;
				case DI8DEVTYPE1STPERSON_SHOOTER:
					type_name = "Shooter";
					break;
				case DI8DEVTYPE1STPERSON_SIXDOF:
					type_name = "Six degrees of freedom";
					break;
				case DI8DEVTYPE1STPERSON_UNKNOWN:
					type_name = "Unknown";
					break;
			}
			break;
		case DI8DEVTYPE_DEVICE:
			type_name = "Unknown class";
			break;
		case DI8DEVTYPE_DEVICECTRL:
			switch (subtype) {
				case DI8DEVTYPEDEVICECTRL_COMMSSELECTION:
					type_name = "Communications selections";
					break;
				case DI8DEVTYPEDEVICECTRL_COMMSSELECTION_HARDWIRED:
					type_name = "Default configuration";
					break;
				case DI8DEVTYPEDEVICECTRL_UNKNOWN:
					type_name = "Unknown";
					break;
			}
			break;
		case DI8DEVTYPE_DRIVING:
			switch (subtype) {
				case DI8DEVTYPEDRIVING_COMBINEDPEDALS:
					type_name = "Acceleration and brake pedal values from a single axis";
					break;
				case DI8DEVTYPEDRIVING_DUALPEDALS:
					type_name = "Acceleration and brake pedal values from separate axes";
					break;
				case DI8DEVTYPEDRIVING_HANDHELD:
					type_name = "Handheld steering device";
					break;
				case DI8DEVTYPEDRIVING_LIMITED:
					type_name = "Limited device";
					break;
				case DI8DEVTYPEDRIVING_THREEPEDALS:
					type_name = "Separate acceleration, brake, and clutch pedal values from separate axes";
					break;
			}
			break;
		case DI8DEVTYPE_FLIGHT:
			switch (subtype) {
				case DI8DEVTYPEFLIGHT_LIMITED:
					type_name = "Limited device";
					break;
				case DI8DEVTYPEFLIGHT_RC:
					type_name = "Remote control for model aircraft";
					break;
				case DI8DEVTYPEFLIGHT_STICK:
					type_name = "Joystick";
					break;
				case DI8DEVTYPEFLIGHT_YOKE:
					type_name = "Yoke";
					break;
			}
			break;
		case DI8DEVTYPE_GAMEPAD:
			switch (subtype) {
				case DI8DEVTYPEGAMEPAD_LIMITED:
					type_name = "Limited device";
					break;
				case DI8DEVTYPEGAMEPAD_STANDARD:
					type_name = "Standard device";
					break;
				case DI8DEVTYPEGAMEPAD_TILT:
					type_name = "Device with x-axis and y-axis data based on inclination";
					break;
			}
			break;
		case DI8DEVTYPE_JOYSTICK:
			switch (subtype) {
				case DI8DEVTYPEJOYSTICK_LIMITED:
					type_name = "Limited device";
					break;
				case DI8DEVTYPEJOYSTICK_STANDARD:
					type_name = "Standard device";
					break;
			}
			break;
		case DI8DEVTYPE_KEYBOARD:
			switch (subtype) {
				case DI8DEVTYPEKEYBOARD_PCAT:
					type_name = "IBM PC/AT 84-key keyboard.";
					break;
				case DI8DEVTYPEKEYBOARD_PCENH:
					type_name = "IBM PC Enhanced 101/102-key";
					break;
			}
			break;
		case DI8DEVTYPE_MOUSE:
			switch (subtype) {
				case DI8DEVTYPEMOUSE_ABSOLUTE:
					type_name = "Mouse that returns absolute axis data";
					break;
				case DI8DEVTYPEMOUSE_FINGERSTICK:
					type_name = "Fingerstick";
					break;
				case DI8DEVTYPEMOUSE_TOUCHPAD:
					type_name = "Touchpad";
					break;
				case DI8DEVTYPEMOUSE_TRACKBALL:
					type_name = "Trackball";
					break;
				case DI8DEVTYPEMOUSE_TRADITIONAL:
					type_name = "Traditional mouse";
					break;
				case DI8DEVTYPEMOUSE_UNKNOWN:
					type_name = "Subtype could not be determined";
					break;
			}
			break;
		case DI8DEVTYPE_REMOTE:
			switch (subtype) {
				case DI8DEVTYPEREMOTE_UNKNOWN:
					type_name = "Unknown";
					break;
			}
			break;
		case DI8DEVTYPE_SCREENPOINTER:
			switch (subtype) {
				case DI8DEVTYPESCREENPTR_LIGHTGUN:
					type_name = "Light gun";
					break;
				case DI8DEVTYPESCREENPTR_LIGHTPEN:
					type_name = "Light pen";
					break;
				case DI8DEVTYPESCREENPTR_TOUCH:
					type_name = "Touch screen";
					break;
				case DI8DEVTYPESCREENPTR_UNKNOWN:
					type_name = "Unknown";
					break;
			}
			break;
		case DI8DEVTYPE_SUPPLEMENTAL:
			switch (subtype) {
				case DI8DEVTYPESUPPLEMENTAL_2NDHANDCONTROLLER:
					type_name = "Secondary handheld controller";
					break;
				case DI8DEVTYPESUPPLEMENTAL_COMBINEDPEDALS:
					type_name = "Acceleration and brake pedal values from a single axis";
					break;
				case DI8DEVTYPESUPPLEMENTAL_DUALPEDALS:
					type_name = "Acceleration and brake pedal values from separate axes";
					break;
				case DI8DEVTYPESUPPLEMENTAL_HANDTRACKER:
					type_name = "Hand movement tracker";
					break;
				case DI8DEVTYPESUPPLEMENTAL_HEADTRACKER:
					type_name = "Head movement tracker";
					break;
				case DI8DEVTYPESUPPLEMENTAL_RUDDERPEDALS:
					type_name = "Rudder pedals";
					break;
				case DI8DEVTYPESUPPLEMENTAL_SHIFTER:
					type_name = "Gear selection from an axis";
					break;
				case DI8DEVTYPESUPPLEMENTAL_SHIFTSTICKGATE:
					type_name = "Gear selection from button states";
					break;
				case DI8DEVTYPESUPPLEMENTAL_SPLITTHROTTLE:
					type_name = "Two throttle values";
					break;
				case DI8DEVTYPESUPPLEMENTAL_THREEPEDALS:
					type_name = "Acceleration, brake, and clutch pedal values from separate axes";
					break;
				case DI8DEVTYPESUPPLEMENTAL_THROTTLE:
					type_name = "Throttle pedal";
					break;
				case DI8DEVTYPESUPPLEMENTAL_UNKNOWN:
					type_name = "Unknown subtype";
					break;
			}
			break;
	}
	return type_name;
}

void
device::readData(){
	poll(&joystickState);
}
