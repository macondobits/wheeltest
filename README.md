# README #

Wheeltest program.

Shows information from Windows controllers using Direct Input.

## Run the program

Just download wheeltest.exe file from repository downloads

## Configuration

Use Cmake to create makefiles and compile the software

```bash
mkdir build
cd build
cmake ..
cmake --build . --target Release
```

## Dependencies

* Visual C++ 2015 Build tools or newer
* Cmake 3.14
* Windows platform SDK
* DirectX SDK
* wxWidgets 3.0 or newer
