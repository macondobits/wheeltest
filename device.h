#define DIRECTINPUT_VERSION 0x0800

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

// for all others, include the necessary headers (this file is usually all you
// need because it includes almost all "standard" wxWidgets headers)
#ifndef WX_PRECOMP
    #include "wx/wx.h"
#endif

#ifdef _WIN32
#include <concrt.h>
#include <dinput.h>
#endif

extern DWORD devtype;
extern DWORD subtype;

extern wxString devInstanceName;
extern wxString devProductName;

extern HWND hWnd;

WX_DECLARE_OBJARRAY(DIDEVICEOBJECTINSTANCE, devObjectArray);

extern devObjectArray devObjects;
extern DIJOYSTATE2 joystickState;

class device {
public:
	int number;
	unsigned int dtype;
	unsigned int dsubtype;
	device(void);
	~device();
	bool hasJoystick();
	void getCapabilities();
	unsigned long getSize();
	unsigned long getFlags();
	unsigned long getDevType();
	unsigned long getAxes();
	unsigned long getButtons();
	unsigned long getPOVs();
	unsigned long getFFSamplePeriod();
	unsigned long getFFMinTimeResolution();
	unsigned long getFirmwareRevision();
	unsigned long getHardwareRevision();
	unsigned long getFFDriverVersion();

	bool hasForceFeedback();
	bool needsPolling();
	void readData();
	wxString getTypeName();
	wxString getSubTypeName();
};

